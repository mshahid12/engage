@extends('layouts.master')

@section('title')
    Welcome
@endsection

@section('content')
    @include('includes.message-block')
    <div class="row">
        <div class="col-md-6 col-md-offset-4">
            <br><br><br><br>
            <h3 align="center" id="testtext">Sign In</h3>
            <form action="{{ route('signin') }}" method="post">
                <div class="form-group{{ $errors->has('email') ? 'has-error' : ''  }}" >
                    <label for="email">Your Email</label>
                    <input class="form-control" type="text" name="email" id="email">
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''  }}" >
                    <label for="password">Password</label>
                    <input class="form-control" type="password" name="password" id="password">
                </div>
                <button type="submit" class="btn btn-primary">Log In</button>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>

            <form action="{{ route('signuppage') }}" method="get">
                <button type="submit" class="btn btn-primary">Register</button>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>

        </div>
    </div>
@endsection

<style>
    form { display: inline; }
</style>
