@extends('layouts.master')

@section('title')
    Welcome
@endsection

@section('content')
    @include('includes.message-block')
    <div class="col-md-6 col-md-offset-4">
        <br><br><br><br>
        <h3 align="center">Sign Up</h3>
        <br>
        <form action="{{ route('signup') }}" method="post">
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''  }}">
                <label for="email">Your Email</label>
                <input class="form-control" type="text" name="email" id="email" value="{{Request::old('email')}}">
            </div>
            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''  }}">
                <label for="first_name">First Name</label>
                <input class="form-control" type="text" name="first_name" id="first_name" >
            </div>
            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''  }}">
                <label for="last_name">Last Name</label>
                <input class="form-control" type="text" name="last_name" id="last_name">
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''  }}">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
    </div>
@endsection
