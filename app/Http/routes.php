<?php

Route::get('/', function () {
    return view('welcome');
})->name('home');
Route::post('/signup', [
    'uses' => 'UserController@postSignUp',
    'as' => 'signup'
]);
Route::post('/signin', [
    'uses' => 'UserController@postSignIn',
    'as' => 'signin'
]);
Route::get('/dashboard', [
    'uses' => 'PostsController@getDashboard',
    'as' => 'dashboard',
    'middleware' => 'auth'
]);
Route::post('/createpost', [
    'uses' => 'PostsController@createPost',
    'as' => 'post.create',
    'middleware' => 'auth'
]);
Route::get('/deletepost/{post_id}', [
    'uses' => 'PostsController@getDeletePost',
    'as' => 'post.delete',
    'middleware' => 'auth'
]);

Route::get('/logout', [
    'uses' => 'UserController@getLogout',
    'as' => 'logout'
]);

Route::post('/edit', [
    'uses' => 'PostsController@postEdit',
    'as' => 'edit'
]);

Route::get('/account', [
    'uses' => 'UserController@getACcount',
    'as' => 'account'
]);

Route::post('/updateaccount', [
    'uses' => 'UserController@postSaveAccount',
    'as' => 'account.save'
]);

Route::get('/userimage/{filename}', [
    'uses' => 'UserController@getUserImage',
    'as' => 'account.image'
]);

Route::post('/like', [
    'uses' => 'PostsController@postLikePost',
    'as' => 'like'
]);
Route::get('/signuppage', function(){
    return view('signup');
})->name('signuppage');